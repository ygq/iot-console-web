// // The Vue build version to load with the `import` command
// // (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// 引入element UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App';
// 引入路由
import router from './router';
// 引入状态管理
import store from './vuex/store';
import {
  Message
} from 'element-ui';

import axios from 'axios';
Vue.prototype.$axios = axios;

Vue.config.productionTip = false;

// 使用element UI
Vue.use(ElementUI);
// 过滤器
import * as custom from './utils/util'

Object.keys(custom).forEach(key => {
  Vue.filter(key, custom[key])
})

Vue.prototype.$tableLoading=false;

import ECharts from 'vue-echarts'
import { use } from 'echarts/core'
// 手动引入 ECharts 各模块来减小打包体积

import {
  CanvasRenderer
} from 'echarts/renderers'
import {
  BarChart
} from 'echarts/charts'
import {
  GridComponent,
  TooltipComponent
} from 'echarts/components'

use([
  CanvasRenderer,
  BarChart,
  GridComponent,
  TooltipComponent
]);

// 全局注册组件（也可以使用局部注册）
Vue.component('v-chart', ECharts)

Vue.config.productionTip = false

class OAuth2 {

  getToken(){
    let token=Vue.prototype.accessToken;
    if(!token){
      try{
      token=localStorage.getItem("access_token");
      }catch(err){
        Message({
          message: "cookie已被禁用，请开启cookie再重新打开",
          type: 'error'
        });
      }
      Vue.prototype.accessToken=token;
    }
    return token;
  }

  setToken(token){
    if(token){
      Vue.prototype.accessToken=token;
      localStorage.setItem("access_token",token);
    }
  }

  saveAuthData(data){
    this.setToken(data.access_token);
    localStorage.setItem("auth_data",JSON.stringify(data));
  }

  getAuthData(key){
    let data=localStorage.getItem("auth_data");
    if(data){
      data=JSON.parse(data);
    }
    if(key){
     return data[key];
    }
    return data;
  }

  /**
   * 使用code登录
   */
  codeLogin(callback){
    const arr=location.hash.split(/[&?]/);
    let code="";
    arr.forEach(kv=>{
      const keyVal=kv.split("=");
      if(keyVal[0]=="code"){
        code=keyVal[1];
      }
    })
    if(code){
      let cookie={};
      if(document.cookie){
        cookie=eval("({"+document.cookie.replaceAll("=", ":'").replaceAll(";", "',")+"'})");
      }
      const token=cookie['token'];

      if(token && token!="undefined"){
        this.setToken(token);
        this.checkLogin(callback);
        return;
      }

      const authUrl=process.env.VUE_APP_AUTH_URL;
      const clientId=process.env.VUE_APP_AUTH_CLIENTID;
      axios({
        method: "GET",
        url: `${authUrl}/codeLogin?code=${code}&clientId=${clientId}`,
      }).then(res => {
        console.log(JSON.stringify(res));
        if(res.status==200){
          const data=res.data;
          if(data.code==200){
            this.saveAuthData(data.data);
          }
        }
        this.checkLogin(callback);
      })
      .catch(err => {
        console.log(err);
        this.checkLogin(callback);
      });
    }else{
      this.checkLogin(callback);
    }
  }

  getLoginUri(){
  const authUrl=process.env.VUE_APP_AUTH_URL;
  return `${authUrl}/authorize?response_type=code&client_id=iotkit&redirect_uri=${encodeURIComponent(location.href)}&scope=userinfo`
  }

  toLogin(){
    location.href=this.getLoginUri();
  }

  logout(){
    const authUrl=process.env.VUE_APP_AUTH_URL;
    location.href=`${authUrl}/logout?accessToken=${this.getToken()}&redirect_uri=${encodeURIComponent(this.getLoginUri())}`;
  }

  //登录检查
  checkLogin(callback){
    const authUrl=process.env.VUE_APP_AUTH_URL;
    axios({
      method: "GET",
      url: authUrl + "/checkLogin",
    }).then(res => {
      console.log(JSON.stringify(res));
      if(res.status==200){
        let data=res.data;
        if(data.code==200){
          this.saveAuthData(data.data);
          callback();
          const url=location.href;
          const rawUrl=url.replace(/[&]?code=[^&]*/,"").replace("?&","?");
          if(rawUrl!=url){
            location.replace(rawUrl);
          }
          return;
        }
      }
      this.toLogin();
    })
    .catch(err => {
      console.log(err);
      callback();
    });
  }
}
const oauth2=new OAuth2();
Vue.prototype.oauth2=oauth2;

oauth2.codeLogin(()=>{
  return new Vue({
    el: '#app',
    router,
    store, //使用store vuex状态管理
    components: {
      App
    },
    template: '<App/>',
    data: {
      // 空的实例放到根组件下，所有的子组件都能调用
      Bus: new Vue()
    }
  });
});